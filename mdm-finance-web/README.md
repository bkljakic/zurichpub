Module mdm-finance

Release 1.0.0-SNAPSHOT

Known bugs: some types are compatible with the mandatory behavior due to the fact that not all built-in widgets can be recreated by API.

Trello (for (bug) tracking and everything else): https://trello.com/mdm-finance

Slack : https://orchestranetworks.slack.com/messages/[TO BE SET]